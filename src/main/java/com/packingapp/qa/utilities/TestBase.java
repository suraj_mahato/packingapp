
package com.packingapp.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.android.AndroidDriver;

public class TestBase {

	public static ThreadLocal<AndroidDriver> tdriver = new ThreadLocal<AndroidDriver>();

	public static AndroidDriver driver;
	public static Properties prop;
	public static Logger log = Logger.getLogger(TestBase.class);

	public static Properties Config = new Properties();
	public static FileInputStream fis;

	public static FileInputStream input;
	public Boolean Result;
	public static Properties prop_OR;

	protected static ExtentTest test;
	protected static ExtentReports report;
	public String propFileName;
	public InputStream input1;

	public TestBase() {

		try {

			prop = new Properties();

			FileInputStream ip = new FileInputStream(
					"/home/dell/Downloads/packingapp" + "/src/main/resources/properties/Config.properties");

			prop.load(ip);

		} catch (FileNotFoundException e) {
			System.out.println("File Not found");
			e.printStackTrace();

		} catch (IOException e) {
			System.out.println("Io Exception");
			e.printStackTrace();

		}
	}

	public AndroidDriver initiallization() throws MalformedURLException {

		File app_lac = new File(prop.getProperty("appPath"));

		File app = new File(app_lac, prop.getProperty("application"));

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("device", prop.getProperty("deviceA"));
		caps.setCapability("automationName", prop.getProperty("automationName1"));
		caps.setCapability("deviceName", prop.getProperty("deviceNameR"));
		caps.setCapability("udid", prop.getProperty("udidR"));
		caps.setCapability("platformName", prop.getProperty("platformNameA"));
		caps.setCapability("app", app.getAbsolutePath());
		caps.setCapability("autoGrantPermission", true);
		caps.setCapability("skipDeviceInitialization", true);
		caps.setCapability("skipServerInstallation", true);
		caps.setCapability("noReset", true);

		caps.setCapability("appPackage", prop.getProperty("appPackage1"));
		caps.setCapability("appWaitActivity", prop.getProperty("appWaitActivity"));

		driver = new AndroidDriver(new URL(prop.getProperty("appiumServer")), caps);

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		tdriver.set(driver);

		return getDriver();

	}

	public static synchronized AndroidDriver getDriver() {
		return tdriver.get();
	}

	public void startReport() {
		// ExtentReports(String filePath,Boolean replaceExisting)
		// filepath - path of the file, in .htm or .html format - path where your report
		// needs to generate.
		// replaceExisting - Setting to overwrite (TRUE) the existing file or append to
		// it
		// True (default): the file will be replaced with brand new markup, and all
		// existing data will be lost. Use this option to create a brand new report
		// False: existing data will remain, new tests will be appended to the existing
		// report. If the the supplied path does not exist, a new file will be created.
		report = new ExtentReports(System.getProperty("user.dir") + "/test-output/CRMExtentReport.html", true);
		// extent.addSystemInfo("Environment","Environment Name")
		report.addSystemInfo("Host Name", "Country Delight").addSystemInfo("Environment", "Packing App")
				.addSystemInfo("User Name", "Suman Dass");
		// loading the external xml file (i.e., extent-config.xml) which was placed
		// under the base directory
		// You could find the xml file below. Create xml file in your project and copy
		// past the code mentioned below
		report.loadConfig(new File(System.getProperty("user.dir") + "//extent-config.xml"));
	}

	/** Setting OR properties file code */
	public void setORprop() throws Exception {
		try {
			Result = false;
			prop_OR = new Properties();
			propFileName = "OR.properties";

			input1 = new FileInputStream(
					"/home/dell/Downloads/packingapp" + "/src/main/resources/properties/OR.properties");

			if (input1 != null) {
				prop_OR.load(input1);

			} else {
				throw new FileNotFoundException("Property File" + propFileName + "not found in the classpath");
			}
			Result = true;
		} catch (Exception e) {
			Reporter.log("Exception" + e);
		}
	}
}
