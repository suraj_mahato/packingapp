package com.packingapp.qa.utilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;



public class FunctionClass extends TestBase {
	String tcName = new Throwable().getStackTrace()[0].getMethodName(); 


	/** Reading Data from Excel code */
	public static List<String> readingFromExcel(String sheetName, String columnName) throws IOException {
        List<String> data = new ArrayList<>();
        Workbook wb = null;
        try {
               FileInputStream fis = new FileInputStream("./TestData.xlsx");
               wb = WorkbookFactory.create(fis);
               Sheet sh = wb.getSheet(sheetName);
               Iterator<Row> rowIterator = sh.iterator();

               while (rowIterator.hasNext()) {
                     Row row = rowIterator.next();
                     Iterator<Cell> cellIterator = row.cellIterator();

                     while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            if (cell.toString().equalsIgnoreCase(columnName)) {
                                   int rowNum = cell.getRowIndex();
                                   for (int i = rowNum; i <= rowNum; i++) {
                                          for (int j = 1; j < sh.getRow(rowNum).getLastCellNum(); j++) {
                                                 Row row1 = sh.getRow(i);
                                                 Cell cell1 = row1.getCell(j);
                                                 data.add(cell1.toString());
                                          }
                                   }
                            }
                     }
               }
        } catch (Exception e) {
        	Reporter.log("Something went wrong fetching data from Excel" + e);
        } finally {
               wb.close();
        }
        return data;
 } 
/*public ArrayList<String> readingFromExcel(String sheetName, String columnName) {
		ArrayList<String> data = new ArrayList<String>();
		try {
			FileInputStream fis = new FileInputStream("./TestData.xlsx");
			Workbook wb = WorkbookFactory.create(fis);
			Sheet sh = wb.getSheet(sheetName);
			Iterator<Row> rowIterator = sh.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if (cell.toString().equalsIgnoreCase(columnName)) {
						int rowNum = cell.getRowIndex();
						for (int i = rowNum; i <= rowNum; i++) {
							for (int j = 1; j < sh.getRow(rowNum).getLastCellNum(); j++) {
								Row row1 = sh.getRow(i);
								Cell cell1 = row1.getCell(j);
								data.add(cell1.toString());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Something went wrong while fetching data from excel: </font><br>" + e);
		}
		return data;
	}
*/
	/** Method used to wait until element is found */
	public static void waitUntilFound(String xpath, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		try {
			if (xpath != null) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element or Xpath not found on page: </font><br>" + e);
		}
	}

	/** Method used to click on element using xpath */
	public static void clickElement(String xpath) {
		try {
			if (xpath != null) {
				driver.findElement(By.xpath(xpath)).click();
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	}

	/** Method used to click on element using web element */
	public static void clickElement(WebElement element) {
		try {
			if (element != null) {
				element.click();
			} else {
				Reporter.log("element is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	}

	/** Method used to create an element */
	public static WebElement createElement(String xpath) {
		WebElement element = null;
		try {
			if (xpath != null) {
				element = driver.findElement(By.xpath(xpath));
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element not found for the mentioned Xpath: </font><br>" + e);
		}
		return element;
	}

	/** Method used to send keys to element */
	public void sendText(WebElement element, String text) {
		try {
			if (element != null && text != null) {
				element.sendKeys(text);
			} else {
				Reporter.log("Element or Text is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element or Text is not present: </font><br>" + e);
		}
	}

	/** Method used to send keys to element using xpath */
	public void sendText(String xpath, String text) {
		try {
			if (xpath != null && text != null) {
				driver.findElement(By.xpath(xpath)).sendKeys(text);
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element or Text is not present: </font><br>" + e);
		}
	}

	/** Method used to check if element is visible using xpath */
	public boolean elementVisible(String xpath) {
		boolean result = false;
		try {
			if (xpath != null) {
				result = driver.findElement(By.xpath(xpath)).isDisplayed();
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element is not present on page: </font><br>" + e);
		}
		return result;
	}

	/** Method used to check if element is visible using element */
	public boolean elementVisible(WebElement element) {
		boolean result = false;
		try {
			if (element != null) {
				result = element.isDisplayed();
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>Element is not present on page: </font><br>" + e);
		}
		return result;
	}
	
	public void clearText(String xpath) {
	WebElement toClear = driver.findElement(By.xpath(xpath));
	toClear.sendKeys(Keys.CONTROL + "a");
	toClear.sendKeys(Keys.DELETE);
	}
	
	/** This function is used to capture pass screenshots **/
	public static String passScreenCapture(String tcName) throws IOException {
		String dest = null;
		try {
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			dest = "/home/dell/Downloads/packingapp/Screenshots/Pass/" + sdf.format(d) + tcName + " -Verified.png";
			FileUtils.copyFile(scrFile, new File(dest));
		} catch (Exception e) {
			test.log(LogStatus.ERROR, e);
		}
		return dest;
	}

	/** This function is used to capture failed screenshots **/
	public static String failScreenCapture(String tcName) throws IOException {
		String dest = null;
		try {
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HHmmss");
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			dest = "/home/dell/Downloads/packingapp/Screenshots/Error/" + sdf.format(d) + tcName + " -Failed.png";
			FileUtils.copyFile(scrFile, new File(dest));
		} catch (Exception e) {
			test.log(LogStatus.ERROR, e);
		}
		return dest;
	} 
	/** Method use to delete all existing screenshots */
	public static void deleteScreenshots(String path) {
		File dir = new File(path);
		File[] dirContents = dir.listFiles();
		for (int i = 0; i < dirContents.length; i++) {
			if ((dirContents[i].getName().contains("Failed")) || (dirContents[i].getName().contains("Verified"))) {
				dirContents[i].delete();
			}
		}
	} 
	/** Method use to upload File*/
	public static void testUpload() throws InterruptedException
	{
		WebElement element = driver.findElement(By.xpath(".//input[@type='file']"));
		element.click();
		uploadFile("/home/countrydelight/Desktop/123.png");
		Thread.sleep(2000);
	}
	
	/**
     * This method will set any parameter string to the system's clipboard.
     */
	public static void setClipboardData(String string) {
		//StringSelection is a class that can be used for copy and paste operations.
		   StringSelection stringSelection = new StringSelection(string);
		   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		}
	
	/** Method use to upload File*/
	public static void uploadFile(String fileLocation) {
        try {
        	//Setting clipboard with file location
            setClipboardData(fileLocation);
            //native key strokes for CTRL, V and ENTER keys
            Robot robot = new Robot();
	
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
        } catch (Exception exp) {
        	exp.printStackTrace();
        }
    
    
	}
	
	
	
	//Method used to scroll down to the element using xpath
	public static void ScrollToElement(String text) {
		try {
			if (text != null) {
				MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
						+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"" + text + "\"));");
			
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	}

	
	/** Method used to click on element using xpath */
	public static void CopyCrateId(String xpath, String Xpath1) {
		try {
			if (xpath != null && Xpath1 != null) {
				
		
				String CrateId = driver.findElement(By.xpath(xpath)).getText();
				System.out.println("create id: "+CrateId);
				
		    	int createIdtxt= Integer.parseInt(CrateId);
				
				System.out.println("total no: "+createIdtxt);
				
				driver.findElement(By.xpath(Xpath1)).click();
         
			
				Actions s = new Actions(driver);
			    s.sendKeys(String.valueOf(createIdtxt));
				s.sendKeys(Keys.ENTER);
				s.perform();	
			
				
			} else {
				Reporter.log("Xpath is null");
			}
		} catch (Exception e) {
			Reporter.log("<font color='red'>No Element or Xpath found to click: </font><br>" + e);
		}
	}
	

}
