package com.packingapp.qa.testsuite;

import java.io.IOException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import com.packingapp.qa.pages.ClosingStockReturnSummary;
import com.packingapp.qa.pages.Home;
import com.packingapp.qa.pages.Settings;
import com.packingapp.qa.pages.SkuPackaging;
import com.packingapp.qa.pages.Summary;
import com.packingapp.qa.utilities.TestBase;
import com.relevantcodes.extentreports.LogStatus;

import org.apache.poi.ss.formula.ptg.MemErrPtg;
import com.relevantcodes.extentreports.ExtentReports;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;


public class RegressionSuite extends TestBase {

	String failed = "<font color='red'>Test Case Failed</font>";

	@BeforeSuite
	public void initialize() throws IOException {

		initiallization();

	}

	@BeforeTest
	public void Report() {
		startReport();
	}
	
	

	@Test(priority = 0)
	public void verifySettingsPg() {
		try {
			test = report.startTest("Settings Pg "); 
			test.log(LogStatus.PASS, "Settings Method Called"); 
			Settings setting = new Settings();
			setting.SettingsPg();
			Assert.assertTrue(setting.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Settings Failed"); 
		}
		
	}
	

	@Test(priority = 1)
	public void ClosingStockReturnSummaryPg() {
		try {
			test = report.startTest("Closing Stock Return Summary end to end flow"); 
			test.log(LogStatus.PASS, "Closing Stock Return Summary end to end flow Method Called"); 
			ClosingStockReturnSummary csrs = new ClosingStockReturnSummary();
			csrs.ClosingStockSReturnSummaryPg();
			Assert.assertTrue(csrs.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Closing Stock Return Summary end to end flow Failed"); 
		}
		
	}
	
	
	@Test(priority = 2)
	public void ReturnPg() {
		try {
			test = report.startTest("Return end to end Flow "); 
			test.log(LogStatus.PASS, "Return end to end flow Method Called"); 
			ClosingStockReturnSummary returns = new ClosingStockReturnSummary();
			returns.ReturnPg();
			Assert.assertTrue(returns.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Return end to end Flow Failed"); 
		}
		
	}
	

	@Test(priority = 3)
	public void ClosingStockPg() {
		try {
			test = report.startTest("Closing Stock end to end Flow "); 
			test.log(LogStatus.PASS, "Closing Stock end to end flow Method Called"); 
			ClosingStockReturnSummary closingtock = new ClosingStockReturnSummary();
			closingtock.ClosingStockPg();
			Assert.assertTrue(closingtock.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Closing Stock end to end Flow Failed"); 
		}
		
	}
	

	

/*	
	@Test(priority = 0)
	public void verifySkuPackagingPg() {
		try {
			test = report.startTest("SKU PACKAGING end to end flow"); 
			test.log(LogStatus.PASS, "SKU PACKAGING end to end flow Method Called"); 
			SkuPackaging skupack = new SkuPackaging();
			skupack.SKUPackagingPg();
			Assert.assertTrue(skupack.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "SKU PACKAGING end to end flow Failed"); 
		}
		
	}
	
	
	@Test(priority = 1)
	public void verifyCratesAutoDispatchPg() {
		try {
			test = report.startTest("Crates Auto Dispatch"); 
			test.log(LogStatus.PASS, "Crates Auto Dispatch Method Called"); 
			SkuPackaging autodispatch = new SkuPackaging();
			autodispatch.CratesAutoDispatch();
			Assert.assertTrue(autodispatch.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Crates Auto Dispatch Failed"); 
		}
		
	}
	
	
	@Test(enabled=false,priority = 2)
	public void verifyScanCratesPg() {
		try {
			test = report.startTest("Scan Crates"); 
			test.log(LogStatus.PASS, "Scan Crates Method Called"); 
			SkuPackaging scancrate = new SkuPackaging();
			scancrate.ScanCrates();
			Assert.assertTrue(scancrate.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Scan Crates Failed"); 
		}
		
	}
	
/*	
	

	
	
	
/*	
	
	@Test(enabled=false,priority = 0)
	public void verifySettingsPg() {
		try {
			test = report.startTest("Settings Pg "); 
			test.log(LogStatus.PASS, "Settings Method Called"); 
			Settings setting = new Settings();
			setting.SettingsPg();
			Assert.assertTrue(setting.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Settings Failed"); 
		}
		
	}
	

	@Test(enabled=false,priority = 1)
	public void verifyScanQRPg() {
		try {
			test = report.startTest("Scan QR "); 
			test.log(LogStatus.PASS, "Scan QR Method Called"); 
			Settings scanqr = new Settings();
			scanqr.ScanQRPg();
			Assert.assertTrue(scanqr.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Scan QR Failed"); 
		}
		
	}
	

	@Test(enabled=false,priority = 2)
	public void verifyPrinterSetUpPg() {
		try {
			test = report.startTest("Printer SetUp "); 
			test.log(LogStatus.PASS, "Printer SetUp Method Called"); 
			Settings Printer = new Settings();
			Printer.PrinterSetUpPg();
			Assert.assertTrue(Printer.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Printer setup Failed"); 
		}
		
	}
	


	@Test(enabled=false,priority = 3)
	public void verifyScanDispatchPg() {
		try {
			test = report.startTest("Scan Dispatch "); 
			test.log(LogStatus.PASS, "Scan Dispatch Method Called"); 
			Settings Scan = new Settings();
			Scan.ScanDispatchPg();
			Assert.assertTrue(Scan.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Scan Dispatch Failed"); 
		}
		
	}
	

	@Test(enabled=false,priority = 4)
	public void verifyProductThresholdPg() {
		try {
			test = report.startTest("Product Threshold "); 
			test.log(LogStatus.PASS, "Product Threshold Method Called"); 
			Settings ProductThld = new Settings();
			ProductThld.ProductThresholdPg();
			Assert.assertTrue(ProductThld.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Product Threshold Failed"); 
		}
		
	}
	

	@Test(enabled=false,priority = 5)
	public void verifyUserManualPg() {
		try {
			test = report.startTest("User Manual "); 
			test.log(LogStatus.PASS, "User Manual Method Called"); 
			Settings UserM = new Settings();
			UserM.UserManualPg();
			Assert.assertTrue(UserM.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "User Manual Failed"); 
		}
		
	}
	

	@Test(enabled=false,priority = 6)
	public void verifyLogOutPg() {
		try {
			test = report.startTest("LogOut "); 
			test.log(LogStatus.PASS, "LogOut Method Called"); 
			Settings logt = new Settings();
			logt.LogOutPg();
			Assert.assertTrue(logt.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Logout Failed"); 
		}
		
	}
	
	@Test(enabled=false,priority =7)
	public void verifySummaryPg() {
		try {
			test = report.startTest("Summary Pg"); 
			test.log(LogStatus.PASS, "Summmary Pg Method Called"); 
			Summary sum = new Summary();
			sum.SummaryPg();
			Assert.assertTrue(sum.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Summmary pg Failed"); 
		}
		
	}
	
	
	@Test(enabled=false,priority =8)
	public void verifyTodayOrderSummaryPg() {
		try {
			test = report.startTest("Today Order Summary "); 
			test.log(LogStatus.PASS, "Today Order Summmary Pg Method Called"); 
			Summary orderSum = new Summary();
			orderSum.OrderSummaryPg();
			Assert.assertTrue(orderSum.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "order Summary Failed"); 
		}
		
	}
	
	
	@Test(enabled=false,priority =9)
	public void verifyTodayDispatchSummaryPg() {
		try {
			test = report.startTest("Today Dispatch Summary "); 
			test.log(LogStatus.PASS, "Today Dispatch Summmary Pg Method Called"); 
			Summary dispatchSum = new Summary();
			dispatchSum.DispatchSummaryPg();
			Assert.assertTrue(dispatchSum.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "dispatch Summary Failed"); 
		}
		
	}
	
	
	@Test(enabled=false,priority =10)
	public void verifyTomorrowOrderSummaryPg() {
		try {
			test = report.startTest("Tomorrrow Order Summary "); 
			test.log(LogStatus.PASS, "Tomorrrow Order Summmary Pg Method Called"); 
			Summary ordersumtom = new Summary();
			ordersumtom.OrderSummaryTomorrowPg();
			Assert.assertTrue(ordersumtom.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Tomorrow order Summary Failed"); 
		}
		
	}
	
	
	@Test(enabled=false,priority =11)
	public void verifyTomorrrowDispatchSummaryPg() {
		try {
			test = report.startTest("Tomorrrow Dispatch Summary "); 
			test.log(LogStatus.PASS, "Tomorrrow Dispatch Summmary Pg Method Called"); 
			Summary dispatchSumtom = new Summary();
			dispatchSumtom.DispatchSummaryTomorrowPg();
			Assert.assertTrue(dispatchSumtom.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Tomorrow dispatch Summary Failed"); 
		}
		
	}
		

	@Test(priority =12)
	public void verifyHomePg() {
		try {
			test = report.startTest("Home Pg"); 
			test.log(LogStatus.PASS, "Home Pg Method Called"); 
			Home h = new Home();
			h.HomePg();
			Assert.assertTrue(h.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Home pg Failed"); 
		}
		
	}
	

	@Test(priority =13)
	public void verifyHomeTodayTimeSlotPg() {
		try {
			test = report.startTest("Today Timeslot "); 
			test.log(LogStatus.PASS, "Today Timeslot Method Called"); 
			Home TT = new Home();
			TT.TodayTimeSlot();
			Assert.assertTrue(TT.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Today Timeslot Failed"); 
		}
		
	}
	

	@Test(priority =14)
	public void verifyHomeTomorrowTimeSlotPg() {
		try {
			test = report.startTest("Tomorrow Timeslot "); 
			test.log(LogStatus.PASS, "Tomorrow Timeslot Method Called"); 
			Home TTimeslot = new Home();
			TTimeslot.TomorrowTimeSlot();
			Assert.assertTrue(TTimeslot.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Tomorrow Timeslot Failed"); 
		}
		
	}
	
	
	@Test(priority =15)
	public void verifySearchRoutePg() {
		try {
			test = report.startTest("Search Route "); 
			test.log(LogStatus.PASS, "Search Route Method Called"); 
			Home sr = new Home();
			sr.SearchRoute();
			Assert.assertTrue(sr.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Search Route Failed"); 
		}
		
	}
	

	@Test(priority =16)
	public void verifySearchFranchisePg() {
		try {
			test = report.startTest("Select Franchise "); 
			test.log(LogStatus.PASS, "Select Franchise Method Called"); 
			Home sf = new Home();
			sf.SelectFranchise();
			Assert.assertTrue(sf.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Select Franchise Failed"); 
		}
		
	}
	
	@Test(enabled=false,priority =17)
	public void verifyFilterRoutesByAtoZandHighToLow() {
		try {
			test = report.startTest("Filter Routes by A to Z and High To Low "); 
			test.log(LogStatus.PASS, "Filter Routes by A to Z and High To Low Method Called"); 
			Home filtR = new Home();
			filtR.FilterRoutesByAtoZandHighToLow();
			Assert.assertTrue(filtR.Result);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Filter Routes by A to Z and High To Low Failed"); 
		}
		
	}
	
	
*/	

	
	


	@AfterSuite
	public void closeBrowser() {
		// driver.close();
		report.endTest(test);
		report.flush();
	}
}