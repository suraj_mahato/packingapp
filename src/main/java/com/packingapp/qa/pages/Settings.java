package com.packingapp.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.packingapp.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;

public class Settings extends FunctionClass {

	// Page Factory OR:
	
	String SettingsBtn;
	String ScanQRFiled;
	String NavigateBackIcon;
	String PrinterSetUpFiled;
	String ScanDispatchFiled;
	String ProductThreshold;
	String UserManualFiled;
	String LogOutFiled;
	String CancelBtn;
	String SettingsPage;
	String UserManualResult;
	String PrinterSetUpResult;
	String ScanDispatchResult;
	String LogoutResult;

	
	
	/*
	 * This method is Used to View the Settings Page
	 */
	
	public void SettingsPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			SettingsBtn = prop_OR.getProperty("SettingsBtn");
			SettingsPage = prop_OR.getProperty("SettingsPage");
			
			
			waitUntilFound(SettingsBtn, 30);
			clickElement(SettingsBtn);
			
			Thread.sleep(4000);
			
			
			if (elementVisible(SettingsPage)) {
				test.log(LogStatus.PASS,
						"Settings Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Settings Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Settings Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult="Settings";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}
		

	/*
	 * This method is Used to view Scan QR Page
	 */
	
	public void ScanQRPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			ScanQRFiled = prop_OR.getProperty("ScanQRFiled");
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			SettingsPage = prop_OR.getProperty("SettingsPage");
			
			
			waitUntilFound(ScanQRFiled, 30);
			clickElement(ScanQRFiled);
			
			String ExpectedResult="Scan QR";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Scan QR']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
					
			if (elementVisible(ScanQRFiled)) {
				test.log(LogStatus.PASS,
						"ScanQR Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"ScanQR Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"ScanQR Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		waitUntilFound(NavigateBackIcon, 30);
		clickElement(NavigateBackIcon);
		
	}


	/*
	 * This method is Used to View the PrinterSetUp Page
	 */
	
	public void PrinterSetUpPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			PrinterSetUpFiled = prop_OR.getProperty("PrinterSetUpFiled");
			PrinterSetUpResult = prop_OR.getProperty("PrinterSetUpResult");		
			
			waitUntilFound(PrinterSetUpFiled, 30);
			clickElement(PrinterSetUpFiled);
			
			String ExpectedResult="Printer Setup";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Printer Setup']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
			
			
			if (elementVisible(PrinterSetUpResult)) {
				test.log(LogStatus.PASS,
						"PrinterSetUp Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"PrinterSetUp Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"PrinterSetUp Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		waitUntilFound(NavigateBackIcon, 30);
		clickElement(NavigateBackIcon);
		
        
	}
	

	/*
	 * This method is Used to View the Scan Dispatch Page
	 */
	
	public void ScanDispatchPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			ScanDispatchFiled = prop_OR.getProperty("ScanDispatchFiled");
			ScanDispatchResult = prop_OR.getProperty("ScanDispatchResult");
			
			waitUntilFound(ScanDispatchFiled, 30);
			clickElement(ScanDispatchFiled);
				
			String ExpectedResult="Dwarka F and V";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Dwarka F and V']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
	        Thread.sleep(4000);

			
			if (elementVisible(ScanDispatchResult)) {
				test.log(LogStatus.PASS,
						"Scan Dispatch Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Scan Dispatch Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Scan Dispatch Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		
		clickElement(NavigateBackIcon);
		
	}

	/*
	 * This method is Used to View the Product Threshold Page
	 */
	
	public void ProductThresholdPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			ProductThreshold = prop_OR.getProperty("ProductThreshold");
			SettingsPage = prop_OR.getProperty("SettingsPage");
			
			waitUntilFound(ProductThreshold, 30);
			clickElement(ProductThreshold);
			
			
			String ExpectedResult="Product Threshold";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Product Threshold']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
			
			if (elementVisible(ProductThreshold)) {
				test.log(LogStatus.PASS,
						"Product Threshold Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
	
				
			} else {
				test.log(LogStatus.FAIL,
						"Product Threshold Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Product Threshold Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
			
			
		}
		
		
		waitUntilFound(NavigateBackIcon, 30);
		clickElement(NavigateBackIcon);
        
		
	}
	
	
	
	
	/*
	 * This method is Used to View the User Manual Page
	 */
	
	public void UserManualPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			UserManualFiled = prop_OR.getProperty("UserManualFiled");
			UserManualResult = prop_OR.getProperty("UserManualResult");
			
			
			waitUntilFound(UserManualFiled, 30);
			clickElement(UserManualFiled);
			
			
			String ExpectedResult="dev 2.8 - CD Packaging";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='dev 2.8 - CD Packaging']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
			
				
			if (elementVisible(UserManualResult)) {
				test.log(LogStatus.PASS,
						"User Manual Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"User Manual Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"User Manual Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}        
		
		waitUntilFound(NavigateBackIcon, 30);
		clickElement(NavigateBackIcon);
        
		
	}
	


	/*
	 * This method is Used to View the LogOut Page
	 */
	
	public void LogOutPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
	
			LogOutFiled = prop_OR.getProperty("LogOutFiled");
			CancelBtn = prop_OR.getProperty("CancelBtn");
			SettingsPage = prop_OR.getProperty("SettingsPage");
			LogoutResult = prop_OR.getProperty("LogoutResult");
			
			
			waitUntilFound(LogOutFiled, 30);
			clickElement(LogOutFiled);
			
			
			String ExpectedResult="Logout";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Logout']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
			
			
			if (elementVisible(LogoutResult)) {
				test.log(LogStatus.PASS,
						"Logout popup opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Logout popup  not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Logout popup not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		waitUntilFound(CancelBtn, 30);
		clickElement(CancelBtn);
		
		
	}
	

}
