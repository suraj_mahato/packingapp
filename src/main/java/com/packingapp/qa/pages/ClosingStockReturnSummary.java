package com.packingapp.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.packingapp.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class ClosingStockReturnSummary extends FunctionClass {

	// Page Factory OR:

	String SettingsBtn;
	String ClosingStockandReturnLink;
	String ClosingStockandReturnPage;
	String SelectFranchisetextbox;
	String SelectFranchiseDropdown;
	String CSRSummarybtn;
	String ClosingStockBtn;
	String ReturnBtn;
	String ArrowIcon;
	String SelectReasonFiled;
	String SelectReasonDropdown;
	String OkBtn;
	String NextBtn;
	String CompleteCSBtn;
	String SettingsPage;
	String SubmitBtn;
	String ScanIcon;
	String AutoScanPage;
	String ManualEditIcon;
	String ManualScanPage;
	String SearchProductIcon;
	String SearchProductFiled;
	String SelectProductDropdown;
	String WeightFiled;
	String QtyFiled;
	String CSandReturnSummaryPage;
	String PopupSumitCSBtn;
	

	// This method is Used to View the Settings Page

	public void SettingsPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();

			SettingsBtn = prop_OR.getProperty("SettingsBtn");
			SettingsPage = prop_OR.getProperty("SettingsPage");

			waitUntilFound(SettingsBtn, 30);
			clickElement(SettingsBtn);

			Thread.sleep(4000);

			if (elementVisible(SettingsPage)) {
				test.log(LogStatus.PASS,
						"Settings Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Settings Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Settings Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}

		Assert.assertTrue(elementVisible(SettingsPage));

	}

	// This method is Used to View the Closing stock and return Summary Page

	public void ClosingStockSReturnSummaryPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();

			ClosingStockandReturnLink = prop_OR.getProperty("ClosingStockandReturnLink");
			ClosingStockandReturnPage = prop_OR.getProperty("ClosingStockandReturnPage");
			SelectFranchisetextbox = prop_OR.getProperty("SelectFranchisetextbox");
			SelectFranchiseDropdown = prop_OR.getProperty("SelectFranchiseDropdown");
			SubmitBtn = prop_OR.getProperty("SubmitBtn");
			CSRSummarybtn = prop_OR.getProperty("CSRSummarybtn");
			ArrowIcon = prop_OR.getProperty("ArrowIcon");
			SelectReasonFiled = prop_OR.getProperty("SelectReasonFiled");
			SelectReasonDropdown = prop_OR.getProperty("SelectReasonDropdown");
			OkBtn = prop_OR.getProperty("OkBtn");
			NextBtn = prop_OR.getProperty("NextBtn");
			CompleteCSBtn = prop_OR.getProperty("CompleteCSBtn");
			ScanIcon = prop_OR.getProperty("ScanIcon");
			AutoScanPage = prop_OR.getProperty("AutoScanPage");
			ManualEditIcon = prop_OR.getProperty("ManualEditIcon");
			ManualScanPage = prop_OR.getProperty("ManualScanPage");
			SearchProductIcon = prop_OR.getProperty("SearchProductIcon");
			SearchProductFiled = prop_OR.getProperty("SearchProductFiled");
			SelectProductDropdown = prop_OR.getProperty("SelectProductDropdown");
			WeightFiled = prop_OR.getProperty("WeightFiled");

			Thread.sleep(4000);
			clickElement(ClosingStockandReturnLink);

			Assert.assertTrue(elementVisible(ClosingStockandReturnPage));

			Thread.sleep(4000);
			clickElement(SelectFranchisetextbox);

			waitUntilFound(SelectFranchiseDropdown, 30);
			clickElement(SelectFranchiseDropdown);

			waitUntilFound(SubmitBtn, 30);
			clickElement(SubmitBtn);

			Assert.assertTrue(elementVisible(CSRSummarybtn));

			waitUntilFound(CSRSummarybtn, 30);
			clickElement(CSRSummarybtn);

			waitUntilFound(ArrowIcon, 30);
			clickElement(ArrowIcon);

			waitUntilFound(SelectReasonFiled, 30);
			clickElement(SelectReasonFiled);

			waitUntilFound(SelectReasonDropdown, 30);
			clickElement(SelectReasonDropdown);

			Thread.sleep(4000);
			clickElement(OkBtn);

			waitUntilFound(NextBtn, 30);
			clickElement(NextBtn);

			waitUntilFound(CompleteCSBtn, 30);
			clickElement(CompleteCSBtn);

			Thread.sleep(4000);

			if (elementVisible(CSRSummarybtn)) {
				test.log(LogStatus.PASS,
						"Reason added successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Reason added not successfully" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL, "Exception Occured" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}

		Assert.assertTrue(elementVisible(CSRSummarybtn));

	}

	// This method is Used to View the Return Page

	public void ReturnPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();

			ReturnBtn = prop_OR.getProperty("ReturnBtn");
			ScanIcon = prop_OR.getProperty("ScanIcon");
			AutoScanPage = prop_OR.getProperty("AutoScanPage");
			ManualEditIcon = prop_OR.getProperty("ManualEditIcon");
			ManualScanPage = prop_OR.getProperty("ManualScanPage");
			SearchProductIcon = prop_OR.getProperty("SearchProductIcon");
			SearchProductFiled= prop_OR.getProperty("SearchProductFiled");
			SelectProductDropdown = prop_OR.getProperty("SelectProductDropdown");
			WeightFiled = prop_OR.getProperty("WeightFiled");
			QtyFiled = prop_OR.getProperty("QtyFiled");
			CSandReturnSummaryPage = prop_OR.getProperty("CSandReturnSummaryPage");
			SubmitBtn = prop_OR.getProperty("SubmitBtn");
			CSRSummarybtn = prop_OR.getProperty("CSRSummarybtn");
			ArrowIcon = prop_OR.getProperty("ArrowIcon");
			SelectReasonFiled = prop_OR.getProperty("SelectReasonFiled");
			SelectReasonDropdown = prop_OR.getProperty("SelectReasonDropdown");
			OkBtn = prop_OR.getProperty("OkBtn");
			NextBtn = prop_OR.getProperty("NextBtn");
			CompleteCSBtn = prop_OR.getProperty("CompleteCSBtn");


			waitUntilFound(ReturnBtn, 30);
			clickElement(ReturnBtn);

//			Assert.assertTrue(elementVisible(ReturnBtn));

			waitUntilFound(ScanIcon, 30);
			clickElement(ScanIcon);
			
//			Assert.assertTrue(elementVisible(AutoScanPage));

			Thread.sleep(3000);
			clickElement(ManualEditIcon);
			
			Assert.assertTrue(elementVisible(ManualScanPage));

			waitUntilFound(SearchProductIcon, 30);
			clickElement(SearchProductIcon);
			
			waitUntilFound(SearchProductFiled, 30);
			clickElement(SearchProductFiled);	
			
			Thread.sleep(3000);
			sendText(SearchProductFiled, "Onion");
			
			waitUntilFound(SelectProductDropdown, 30);
			clickElement(SelectProductDropdown);
			
			Thread.sleep(3000);
			sendText(QtyFiled, "1");
			
			waitUntilFound(WeightFiled, 30);
			clickElement(WeightFiled);
			
			Thread.sleep(3000);
			sendText(WeightFiled, "1");
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
			
			Thread.sleep(3000);
			clickElement(SubmitBtn);
			
			Thread.sleep(3000);
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

			Thread.sleep(3000);
			clickElement(SubmitBtn);

			Thread.sleep(4000);
			clickElement(ArrowIcon);

			waitUntilFound(SelectReasonFiled, 30);
			clickElement(SelectReasonFiled);

			waitUntilFound(SelectReasonDropdown, 30);
			clickElement(SelectReasonDropdown);

			Thread.sleep(4000);
			clickElement(OkBtn);

			waitUntilFound(NextBtn, 30);
			clickElement(NextBtn);

			waitUntilFound(CompleteCSBtn, 30);
			clickElement(CompleteCSBtn);

			Thread.sleep(4000);

			if (elementVisible(CSRSummarybtn)) {
				test.log(LogStatus.PASS,
						"Reason added successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Reason added not successfully" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL, "Exception Occured" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}

		Assert.assertTrue(elementVisible(CSRSummarybtn));

	}

	
	// This method is Used to do Closing Stock Page

		public void ClosingStockPg() throws InterruptedException, IOException {
			String tcName = new Throwable().getStackTrace()[0].getMethodName();

			try {
				setORprop();

				ClosingStockBtn = prop_OR.getProperty("ClosingStockBtn");
				ScanIcon = prop_OR.getProperty("ScanIcon");
				AutoScanPage = prop_OR.getProperty("AutoScanPage");
				ManualEditIcon = prop_OR.getProperty("ManualEditIcon");
				ManualScanPage = prop_OR.getProperty("ManualScanPage");
				SearchProductIcon = prop_OR.getProperty("SearchProductIcon");
				SearchProductFiled= prop_OR.getProperty("SearchProductFiled");
				SelectProductDropdown = prop_OR.getProperty("SelectProductDropdown");
				WeightFiled = prop_OR.getProperty("WeightFiled");
				QtyFiled = prop_OR.getProperty("QtyFiled");
				CSandReturnSummaryPage = prop_OR.getProperty("CSandReturnSummaryPage");
				SubmitBtn = prop_OR.getProperty("SubmitBtn");
				PopupSumitCSBtn = prop_OR.getProperty("PopupSumitCSBtn");
				CompleteCSBtn = prop_OR.getProperty("CompleteCSBtn");
				CSRSummarybtn = prop_OR.getProperty("CSRSummarybtn");
				ArrowIcon = prop_OR.getProperty("ArrowIcon");
				SelectReasonFiled = prop_OR.getProperty("SelectReasonFiled");
				SelectReasonDropdown = prop_OR.getProperty("SelectReasonDropdown");
				OkBtn = prop_OR.getProperty("OkBtn");
				NextBtn = prop_OR.getProperty("NextBtn");
				CompleteCSBtn = prop_OR.getProperty("CompleteCSBtn");
				

				waitUntilFound(ClosingStockBtn, 30);
				clickElement(ClosingStockBtn);

//				Assert.assertTrue(elementVisible(ReturnBtn));

				waitUntilFound(ScanIcon, 30);
				clickElement(ScanIcon);
				
//				Assert.assertTrue(elementVisible(AutoScanPage));

				Thread.sleep(3000);
				clickElement(ManualEditIcon);
				
				Assert.assertTrue(elementVisible(ManualScanPage));

				waitUntilFound(SearchProductIcon, 30);
				clickElement(SearchProductIcon);
				
				waitUntilFound(SearchProductFiled, 30);
				clickElement(SearchProductFiled);	
				
				Thread.sleep(3000);
				sendText(SearchProductFiled, "Onion");
				
				waitUntilFound(SelectProductDropdown, 30);
				clickElement(SelectProductDropdown);
				
				Thread.sleep(3000);
				sendText(QtyFiled, "1");
				
				waitUntilFound(WeightFiled, 30);
				clickElement(WeightFiled);
				
				Thread.sleep(3000);
				sendText(WeightFiled, "1");
				
				// mobile back
				driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
				
				Thread.sleep(3000);
				clickElement(SubmitBtn);
				
				Thread.sleep(3000);
				
				// mobile back
				driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

				Thread.sleep(3000);
				clickElement(CompleteCSBtn);
				
				Thread.sleep(3000);
				clickElement(PopupSumitCSBtn);
				
				waitUntilFound(ArrowIcon, 30);
				clickElement(ArrowIcon);

				waitUntilFound(SelectReasonFiled, 30);
				clickElement(SelectReasonFiled);

				waitUntilFound(SelectReasonDropdown, 30);
				clickElement(SelectReasonDropdown);

				Thread.sleep(4000);
				clickElement(OkBtn);

				waitUntilFound(NextBtn, 30);
				clickElement(NextBtn);

				waitUntilFound(CompleteCSBtn, 30);
				clickElement(CompleteCSBtn);

				Thread.sleep(4000);

				if (elementVisible(CSRSummarybtn)) {
					test.log(LogStatus.PASS,
							"Reason added successfully" + test.addScreenCapture(passScreenCapture(tcName)));
					;
				} else {
					test.log(LogStatus.FAIL,
							"Reason added not successfully" + test.addScreenCapture(failScreenCapture(tcName)));
				}
			} catch (Exception e) {
				{
					test.log(LogStatus.FAIL, "Exception Occured" + test.addScreenCapture(failScreenCapture(tcName)));
				}
			}

			Assert.assertTrue(elementVisible(CSRSummarybtn));

		}
		
}	
