package com.packingapp.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.packingapp.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;

public class Home extends FunctionClass {

	// Page Factory OR:
	
	String homeBtn;
	String SearchIcon;
	String SearchRoutetxtBox;
	String RouteScreenBackIcon;
	String FilterIcon;
	String FilterRouteAll;
	String TodayBtn;
	String TomorrowBtn;
	String TimeSlotBtn;
	String MrngBtn;
	String EvenBtn;
	String homePage;
	String SelectFranchiseIcon;
	String SelectFranchise1;
	String SelectFranchise2;
	String SelectFranchiseResult;
	String RouteNameFilterBtn;
    String PackLaterFilterBtn;
	String PendingOrderFilterBtn;
	String CancelledOrdersFilterBtn;
	String PackedOrdersFilterBtn;
	
	
	
	/*
	 * This method is Used to View the Home Page
	 */
	
	public void HomePg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			homeBtn = prop_OR.getProperty("homeBtn");
			homePage = prop_OR.getProperty("homePage");
			
			
			waitUntilFound(homeBtn, 30);
			clickElement(homeBtn);
			
			Thread.sleep(4000);
			
			
			if (elementVisible(homePage)) {
				test.log(LogStatus.PASS,
						"home Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"home Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"home Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult= "Total";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Total']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}


	/*
	 * This method is Used to View  for Today TimeSlot 
	 */
	
	public void TodayTimeSlot() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			TodayBtn = prop_OR.getProperty("TodayBtn");
			EvenBtn = prop_OR.getProperty("EvenBtn");
			TimeSlotBtn = prop_OR.getProperty("TimeSlotBtn");
			homePage = prop_OR.getProperty("homePage");

			waitUntilFound(TodayBtn, 30);
			clickElement(TodayBtn);
			
			waitUntilFound(TimeSlotBtn, 30);
			clickElement(TimeSlotBtn);
				
			waitUntilFound(EvenBtn, 30);
			clickElement(EvenBtn);
	        
			Thread.sleep(4000);
			
			
			if (elementVisible(homePage)) {
				test.log(LogStatus.PASS,
						"Selected Today timeslot successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Not Selected Today timeslot successfully" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						" Not Selected Today timeslot successfully" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult= "Total";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Total']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}

	

	/*
	 * This method is Used to View  for Tomorrow TimeSlot 
	 */
	
	public void TomorrowTimeSlot() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			TomorrowBtn = prop_OR.getProperty("TomorrowBtn");
			MrngBtn = prop_OR.getProperty("MrngBtn");
			TimeSlotBtn = prop_OR.getProperty("TimeSlotBtn");
			homePage = prop_OR.getProperty("homePage");

			waitUntilFound(TomorrowBtn, 30);
			clickElement(TomorrowBtn);
			
			waitUntilFound(TimeSlotBtn, 30);
			clickElement(TimeSlotBtn);
				
			waitUntilFound(MrngBtn, 30);
			clickElement(MrngBtn);
	        
			Thread.sleep(4000);
			
			
			if (elementVisible(homePage)) {
				test.log(LogStatus.PASS,
						"Selected Tomorrow timeslot successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Not Selected Tomorrow timeslot successfully" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Not Selected Tomorrow timeslot successfully" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult= "Total";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Total']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}

	

	/*
	 * This method is Used to Search the Route 
	 */
	
	public void SearchRoute() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			SearchIcon = prop_OR.getProperty("SearchIcon");
			SearchRoutetxtBox = prop_OR.getProperty("SearchRoutetxtBox");
			RouteScreenBackIcon = prop_OR.getProperty("RouteScreenBackIcon");
			homePage = prop_OR.getProperty("homePage");

			waitUntilFound(SearchIcon, 30);
			clickElement(SearchIcon);
			
			waitUntilFound(SearchRoutetxtBox, 30);
			clickElement(SearchRoutetxtBox);
			
			String ExpectedResult= "Search Route";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.EditText[@text='Search Route']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
			
			waitUntilFound(SearchRoutetxtBox, 30);
			sendText(SearchRoutetxtBox, "Sunil");
				
	        
	//		Thread.sleep(4000);
			
			
			if (elementVisible(homePage)) {
				test.log(LogStatus.PASS,
						"Search Route functionality is working " + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Search Route functionality is not working" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Search Route functionality is not working" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		waitUntilFound(RouteScreenBackIcon, 30);
		clickElement(RouteScreenBackIcon);
		
		String ExpectedResult= "Total";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Total']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}


	/*
	 * This method is Used to Select the Franchise 
	 */
	
	public void SelectFranchise() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			SelectFranchiseIcon = prop_OR.getProperty("SelectFranchiseIcon");
			SelectFranchise1 = prop_OR.getProperty("SelectFranchise1");
			SelectFranchise2 = prop_OR.getProperty("SelectFranchise2");
			SelectFranchiseResult = prop_OR.getProperty("SelectFranchiseResult");

			waitUntilFound(SelectFranchiseIcon, 30);
			clickElement(SelectFranchiseIcon);
			
			waitUntilFound(SelectFranchise1, 30);
			clickElement(SelectFranchise1);
			
			waitUntilFound(SelectFranchiseIcon, 30);
			clickElement(SelectFranchiseIcon);
			
			waitUntilFound(SelectFranchise2, 30);
			clickElement(SelectFranchise2);
			
			waitUntilFound(SelectFranchiseIcon, 30);
			clickElement(SelectFranchiseIcon);
			
			waitUntilFound(SelectFranchise1, 30);
			clickElement(SelectFranchise1);
	        
			Thread.sleep(4000);
			
			
			if (elementVisible(SelectFranchiseResult)) {
				test.log(LogStatus.PASS,
						"Select Franchisefunctionality is working " + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Select Franchise functionality is not working" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Select Franchise functionality is not working" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult= "Dwarka F and V";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Dwarka F and V']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}
	


	/*
	 * This method is Used to Filter Routes by A to Z and High To Low
	 */
	
	public void FilterRoutesByAtoZandHighToLow() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			FilterIcon = prop_OR.getProperty("FilterIcon");
			RouteNameFilterBtn = prop_OR.getProperty("RouteNameFilterBtn");
			PackLaterFilterBtn = prop_OR.getProperty("PackLaterFilterBtn");
			PendingOrderFilterBtn = prop_OR.getProperty("PendingOrderFilterBtn");
			CancelledOrdersFilterBtn = prop_OR.getProperty("CancelledOrdersFilterBtn");
			PackedOrdersFilterBtn = prop_OR.getProperty("PackedOrdersFilterBtn");
			homePage = prop_OR.getProperty("homePage");

			waitUntilFound(FilterIcon, 30);
			clickElement(FilterIcon);
			
			waitUntilFound(RouteNameFilterBtn, 30);
			clickElement(RouteNameFilterBtn);
			
			waitUntilFound(FilterIcon, 30);
			clickElement(FilterIcon);
			
			waitUntilFound(PackLaterFilterBtn, 30);
			clickElement(PackLaterFilterBtn);
			
			waitUntilFound(FilterIcon, 30);
			clickElement(FilterIcon);
			
			waitUntilFound(PendingOrderFilterBtn, 30);
			clickElement(PendingOrderFilterBtn);
			
			waitUntilFound(FilterIcon, 30);
			clickElement(FilterIcon);
			
			waitUntilFound(CancelledOrdersFilterBtn, 30);
			clickElement(CancelledOrdersFilterBtn);
			
			waitUntilFound(FilterIcon, 30);
			clickElement(FilterIcon);
			
			waitUntilFound(PackedOrdersFilterBtn, 30);
			clickElement(PackedOrdersFilterBtn);
	        
			Thread.sleep(4000);
			
			
			if (elementVisible(homePage)) {
				test.log(LogStatus.PASS,
						" Filter Routes by A to Z and High To Low  is working " + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						" Filter Routes by A to Z and High To Low is not working" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						" Filter Routes by A to Z and High To Low is not working" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult= "Total";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Total']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}
	

	
}






