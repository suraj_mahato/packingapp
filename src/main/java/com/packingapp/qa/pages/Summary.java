package com.packingapp.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.packingapp.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;

public class Summary extends FunctionClass {

	// Page Factory OR:
	
	String SummaryBtn;
	String OrderSummarybtn;
	String DispatchSummarybtn;
	String TodayBtn;
	String TomorrowBtn;
	String TimeSlotBtn;
	String MorningBtn;
	String EveningBtn;
	String LateMorning;
	String SummaryPage;
	String OrderSummaryPage;
	String DisPatchSummaryPage;
    String NavigateBackIcon;
    String OrderSummaryResult;
    String SelectFranchsieResult;
    String TommorrowOrderSummaryResult;
	
	
	/*
	 * This method is Used to View the Summary Page
	 */
	
	public void SummaryPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			SummaryBtn = prop_OR.getProperty("SummaryBtn");
			SummaryPage = prop_OR.getProperty("SummaryPage");
			
			
			waitUntilFound(SummaryBtn, 30);
			clickElement(SummaryBtn);
			
			Thread.sleep(4000);
			
			
			if (elementVisible(SummaryPage)) {
				test.log(LogStatus.PASS,
						"Summary Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		String ExpectedResult= "Summary";
        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Summary']")).getText();
        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}
	

	/*
	 * This method is Used to View  for Today Order Summary Page 
	 */
	
	public void OrderSummaryPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			OrderSummarybtn = prop_OR.getProperty("OrderSummarybtn");
			SummaryPage = prop_OR.getProperty("SummaryPage");
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			TimeSlotBtn = prop_OR.getProperty("TimeSlotBtn");
			EveningBtn = prop_OR.getProperty("EveningBtn");
			TodayBtn = prop_OR.getProperty("TodayBtn");
			OrderSummaryResult = prop_OR.getProperty("OrderSummaryResult");
			
			waitUntilFound(TodayBtn, 30);
			clickElement(TodayBtn);

			waitUntilFound(TimeSlotBtn, 30);
			clickElement(TimeSlotBtn);
			
			waitUntilFound(EveningBtn, 30);
			clickElement(EveningBtn);
				
			waitUntilFound(OrderSummarybtn, 30);
			clickElement(OrderSummarybtn);
			
			String ExpectedResult= "Order Summary";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Order Summary']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
	//		Thread.sleep(4000);
			
			
			if (elementVisible(OrderSummaryResult)) {
				test.log(LogStatus.PASS,
						"Today Order Summary Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Today Order Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Today Order Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		
		 waitUntilFound(NavigateBackIcon, 30);
	   	  clickElement(NavigateBackIcon);
		
			String ExpectedResult= "Summary";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Summary']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}

	

	/*
	 * This method is Used to View  for Today Dispatch Summary Page 
	 */
	
	public void DispatchSummaryPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			DispatchSummarybtn = prop_OR.getProperty("DispatchSummarybtn");
			SummaryPage = prop_OR.getProperty("SummaryPage");
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			TimeSlotBtn = prop_OR.getProperty("TimeSlotBtn");
			MorningBtn = prop_OR.getProperty("MorningBtn");
			SelectFranchsieResult = prop_OR.getProperty("SelectFranchsieResult");
			

			waitUntilFound(TimeSlotBtn, 30);
			clickElement(TimeSlotBtn);
			
			waitUntilFound(MorningBtn, 30);
			clickElement(MorningBtn);
			
			
			waitUntilFound(DispatchSummarybtn, 30);
			clickElement(DispatchSummarybtn);
			
			String ExpectedResult= "Select Franchise";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Select Franchise']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
	        
	//		Thread.sleep(4000);
			
			
			if (elementVisible(SelectFranchsieResult)) {
				test.log(LogStatus.PASS,
						"Today Dispatch Summary Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Today Dispatch Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Today Dispatch Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		
		  waitUntilFound(NavigateBackIcon, 30);
		  clickElement(NavigateBackIcon);
		
			String ExpectedResult= "Summary";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Summary']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
	}

	/*
	 * This method is Used to View for Tommorrow Order Summary Page 
	 */
	
	public void OrderSummaryTomorrowPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			OrderSummarybtn = prop_OR.getProperty("OrderSummarybtn");
			SummaryPage = prop_OR.getProperty("SummaryPage");
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			TimeSlotBtn = prop_OR.getProperty("TimeSlotBtn");
			EveningBtn = prop_OR.getProperty("EveningBtn");
			TomorrowBtn = prop_OR.getProperty("TomorrowBtn");
			TommorrowOrderSummaryResult = prop_OR.getProperty("TommorrowOrderSummaryResult");
			

			waitUntilFound(TomorrowBtn, 30);
			clickElement(TomorrowBtn);

			waitUntilFound(TimeSlotBtn, 30);
			clickElement(TimeSlotBtn);
			
			waitUntilFound(EveningBtn, 30);
			clickElement(EveningBtn);
				
			waitUntilFound(OrderSummarybtn, 30);
			clickElement(OrderSummarybtn);
			
			
			String ExpectedResult= "Total Customer";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Total Customer']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
		//	Thread.sleep(4000);
					
			if (elementVisible(TommorrowOrderSummaryResult)) {
				test.log(LogStatus.PASS,
						"Today Order Summary Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Today Order Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Today Order Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		
	   	  clickElement(NavigateBackIcon);
		
			String ExpectedResult= "Summary";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Summary']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
		
	}

	


	

	/*
	 * This method is Used to View  for Tomorrow Dispatch Summary Page 
	 */
	
	public void DispatchSummaryTomorrowPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			DispatchSummarybtn = prop_OR.getProperty("DispatchSummarybtn");
			SummaryPage = prop_OR.getProperty("SummaryPage");
			NavigateBackIcon = prop_OR.getProperty("NavigateBackIcon");
			TimeSlotBtn = prop_OR.getProperty("TimeSlotBtn");
			MorningBtn = prop_OR.getProperty("MorningBtn");
			SelectFranchsieResult = prop_OR.getProperty("SelectFranchsieResult");
			

			waitUntilFound(TimeSlotBtn, 30);
			clickElement(TimeSlotBtn);
			
			waitUntilFound(MorningBtn, 30);
			clickElement(MorningBtn);
			
			
			waitUntilFound(DispatchSummarybtn, 30);
			clickElement(DispatchSummarybtn);
			
			String ExpectedResult= "Select Franchise";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Select Franchise']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
	        
	//		Thread.sleep(4000);
			
			
			if (elementVisible(SelectFranchsieResult)) {
				test.log(LogStatus.PASS,
						"Today Dispatch Summary Page opened successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Today Dispatch Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL,
						"Today Dispatch Summary Page not opening" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		
		  waitUntilFound(NavigateBackIcon, 30);
		  clickElement(NavigateBackIcon);
		  
			String ExpectedResult= "Summary";
	        String ActualResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Summary']")).getText();
	        Assert.assertEquals(ActualResult, ExpectedResult);
	        
		
	}

}


