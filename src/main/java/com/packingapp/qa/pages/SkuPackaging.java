package com.packingapp.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.packingapp.qa.utilities.FunctionClass;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class SkuPackaging extends FunctionClass {

	// Page Factory OR:
	
	String homeBtn;
	String SKUPackagingBtn;
	String TableBtn;
	String ScanIcon;
	String ManualEditIcon;
	String SearchProductIcon;
	String SearchProductFiled;
	String SelectProductDropdown;
	String WeightFiled;
	String SubmitBtn;
	String MapCrateBtn;
	String PopupMapCrateBtn;
	String ScanCrateTextViewBtn;
	String EnterCrateIdFiled;
	String TablePage;
	String ScanPage;
	String AutoScanPage;
	String ManualScanPage;
	String CrateSummaryPage;
	String CrateIdText;
	String CreateDispatchBtn;
	String GurgoanFNVBtn;
	String SelectFranchisePage;
	String TruckNoFiled;
	String SelectTruckDropdown;
	String DriverNameFiled;
	String SelectDriverNameDropdown;
	String StartDispatchBtn;
	String SelectTruckPage;
	String CompleteDispatchBtn;
	String YesBtn;
	
	
	
	// This method is Used to do SKU Packaging 

	public void SKUPackagingPg() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			SKUPackagingBtn = prop_OR.getProperty("SKUPackagingBtn");
			TablePage = prop_OR.getProperty("TablePage");
			TableBtn = prop_OR.getProperty("TableBtn");
			ScanPage = prop_OR.getProperty("ScanPage");
			ScanIcon = prop_OR.getProperty("ScanIcon");
			AutoScanPage = prop_OR.getProperty("AutoScanPage");
			ManualEditIcon = prop_OR.getProperty("ManualEditIcon");
			ManualScanPage = prop_OR.getProperty("ManualScanPage");
			SearchProductIcon = prop_OR.getProperty("SearchProductIcon");
			SearchProductFiled = prop_OR.getProperty("SearchProductFiled");
			SelectProductDropdown = prop_OR.getProperty("SelectProductDropdown");
			WeightFiled = prop_OR.getProperty("WeightFiled");
			MapCrateBtn = prop_OR.getProperty("MapCrateBtn");
			PopupMapCrateBtn = prop_OR.getProperty("PopupMapCrateBtn");
			CrateSummaryPage = prop_OR.getProperty("CrateSummaryPage");
			SubmitBtn = prop_OR.getProperty("SubmitBtn");
			
			
			
			
			waitUntilFound(SKUPackagingBtn, 40);
			clickElement(SKUPackagingBtn);
			
    		Assert.assertTrue(elementVisible(TablePage));
			
			waitUntilFound(TableBtn, 30);
			clickElement(TableBtn);
			
			Assert.assertTrue(elementVisible(ScanPage));
			
			waitUntilFound(ScanIcon, 30);
			clickElement(ScanIcon);
						
			Thread.sleep(3000);
			clickElement(ManualEditIcon);
			
			Assert.assertTrue(elementVisible(ManualScanPage));
			
			waitUntilFound(SearchProductIcon, 30);
			clickElement(SearchProductIcon);
			
			waitUntilFound(SearchProductFiled, 30);
			clickElement(SearchProductFiled);	
			
			Thread.sleep(3000);
			sendText(SearchProductFiled, "Onion");
			
			waitUntilFound(SelectProductDropdown, 30);
			clickElement(SelectProductDropdown);
			
			waitUntilFound(WeightFiled, 30);
			clickElement(WeightFiled);
			
			Thread.sleep(3000);
			sendText(WeightFiled, "1");
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
			
			Thread.sleep(3000);
			clickElement(SubmitBtn);
			
			Thread.sleep(3000);
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

			waitUntilFound(MapCrateBtn, 30);
			clickElement(MapCrateBtn);
			
			Thread.sleep(4000);
			
			
			if (elementVisible(PopupMapCrateBtn)) {
				test.log(LogStatus.PASS,
						"SKU Packaging should be done successfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"SKU Packaging should not be done sucessfully " + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL, "Exception Occured" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		clickElement(PopupMapCrateBtn);
		
		Assert.assertTrue(elementVisible(CrateSummaryPage));
		
	}
	

	// This method is Used to do Scan the crate
	
	public void ScanCrates() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			ScanCrateTextViewBtn = prop_OR.getProperty("ScanCrateTextViewBtn");
			ManualEditIcon = prop_OR.getProperty("ManualEditIcon");
            ManualScanPage = prop_OR.getProperty("ManualScanPage");
            CrateIdText = prop_OR.getProperty("CrateIdText");
            EnterCrateIdFiled = prop_OR.getProperty("EnterCrateIdFiled");
			SubmitBtn = prop_OR.getProperty("SubmitBtn");
			AutoScanPage = prop_OR.getProperty("AutoScanPage");
			
			
			Thread.sleep(4000);
			clickElement(ScanCrateTextViewBtn);
						
			Thread.sleep(3000);
			clickElement(ManualEditIcon);
			
			Assert.assertTrue(elementVisible(ManualScanPage));
			
			Thread.sleep(3000);
            CopyCrateId(CrateIdText, EnterCrateIdFiled);
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
			
			Thread.sleep(3000);
			clickElement(SubmitBtn);
			
			Thread.sleep(4000);
			
			
			if (elementVisible(AutoScanPage)) {
				test.log(LogStatus.PASS,
						"Scan crate succesfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"Scan crate unsuccesfully " + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL, "Exception Occured" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		Assert.assertTrue(elementVisible(AutoScanPage));
		
	}
	

	// This method is Used to do Crate Dispatch with Auto Dispatch
	
	public void CratesAutoDispatch() throws InterruptedException, IOException {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();

		try {
			setORprop();
			
			CreateDispatchBtn = prop_OR.getProperty("CreateDispatchBtn");
			GurgoanFNVBtn = prop_OR.getProperty("GurgoanFNVBtn");
			SelectFranchisePage = prop_OR.getProperty("SelectFranchisePage");
			SelectTruckPage = prop_OR.getProperty("SelectTruckPage");
			TruckNoFiled = prop_OR.getProperty("TruckNoFiled");
			SelectTruckDropdown = prop_OR.getProperty("SelectTruckDropdown");
			DriverNameFiled = prop_OR.getProperty("DriverNameFiled");
			SelectDriverNameDropdown = prop_OR.getProperty("SelectDriverNameDropdown");
			StartDispatchBtn = prop_OR.getProperty("StartDispatchBtn");
			ScanIcon = prop_OR.getProperty("ScanIcon");
			CompleteDispatchBtn = prop_OR.getProperty("CompleteDispatchBtn");
			YesBtn = prop_OR.getProperty("YesBtn");
			ManualEditIcon = prop_OR.getProperty("ManualEditIcon");
            ManualScanPage = prop_OR.getProperty("ManualScanPage");
            CrateIdText = prop_OR.getProperty("CrateIdText");
            EnterCrateIdFiled = prop_OR.getProperty("EnterCrateIdFiled");
			SubmitBtn = prop_OR.getProperty("SubmitBtn");
			AutoScanPage = prop_OR.getProperty("AutoScanPage");
			
			
			Thread.sleep(15000);
			clickElement(CreateDispatchBtn);
			
			Assert.assertTrue(elementVisible(SelectFranchisePage));
			
			waitUntilFound(GurgoanFNVBtn, 30);
			clickElement(GurgoanFNVBtn);
			
			Assert.assertTrue(elementVisible(SelectTruckPage));
			
			waitUntilFound(TruckNoFiled, 30);
			clickElement(TruckNoFiled);
			
			waitUntilFound(SelectTruckDropdown, 30);
			clickElement(SelectTruckDropdown);		
			
			waitUntilFound(DriverNameFiled, 30);
			clickElement(DriverNameFiled);
			
			waitUntilFound(SelectDriverNameDropdown, 30);
			clickElement(SelectDriverNameDropdown);
			
			waitUntilFound(StartDispatchBtn, 30);
			clickElement(StartDispatchBtn);
			
			waitUntilFound(ScanIcon, 30);
			clickElement(ScanIcon);
			
			Thread.sleep(3000);
			clickElement(ManualEditIcon);
			
			Assert.assertTrue(elementVisible(ManualScanPage));
			
			Thread.sleep(3000);
            sendText(EnterCrateIdFiled, "4");
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
			
			Thread.sleep(3000);
			clickElement(SubmitBtn);
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
			
			Thread.sleep(2000);
			
			// mobile back
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
			
			Thread.sleep(2000);
			clickElement(CompleteDispatchBtn);
			
			Thread.sleep(4000);
			
			
			if (elementVisible(AutoScanPage)) {
				test.log(LogStatus.PASS,
						"crate auto dispatch succesfully" + test.addScreenCapture(passScreenCapture(tcName)));
				;
			} else {
				test.log(LogStatus.FAIL,
						"crate auto dispatch unsuccesfully " + test.addScreenCapture(failScreenCapture(tcName)));
			}
		} catch (Exception e) {
			{
				test.log(LogStatus.FAIL, "Exception Occured" + test.addScreenCapture(failScreenCapture(tcName)));
			}
		}
		
		
		clickElement(YesBtn);
		Assert.assertTrue(elementVisible(AutoScanPage));
		
	}
	
	
}